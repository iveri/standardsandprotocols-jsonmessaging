/// <reference path="RequestReponseIDLDataTypes.ts" />

//
// JSON მესიჯის ობიექტის საბაზო ტიპი
//
// JSONMessage.messageId: 
// გამოიყენება
// - ასინქრონული 1-ზე მეტი ერთი და იგივე მაგრამ სხვა და სხვა პარამეტრების 
//   ქმონე request-ებზე response-ების ფილტრაციისათვის
// - გამოიყენება ტრანზქციული მესიჯინგის რეალიზებისთვის, კერძოდ: უწინ გაგზავნილი 
//   messageId-ის მგონი მესიჯის დამუშავების სისრულის გადამოწმება
// - ასევე ერთი გაგზავნილ request-ზე შეიძლება რამდენიმე სხვადასხვა სახელის მქონე 
//   response მესიჯი მოდიოდეს, აქ კვლავ request.messageId-ი არის ის რის საფუძველზეც შეიძლება request-response-ს შესაბამისობების დადგენა
//
// JSONMessage.type: 
// - ასევე request response-ებს შორის კავშირის დამყარების საშუალებაა, ასინქრონულ რეჟიმში მრავალი request-ის გზავნისას.
// - ერთი და იგივე JSONMessage.messageId მნიშვნელობისათვის, JSONMessage.type-ით ხდება დადგენა, მესიჯი პირველადია თუ პასუხია რაიმე წინ მსწრებ მესიჯზე.
//

interface JSONMessage {
    method: JSONMethod;
    params: JSONMessageParams;
    messageId?: number | string;
    type?: JSONMessageType;
    errorCode?: ErrorCode;
}

interface JSONMessageExt1<T extends JSONMessageParams> extends JSONMessage {
    params: T
}

type LanguageCode = Integer

type LocalizedString = {
    key: LanguageCode
    data: String
}

type JSONMethod = Integer 

type ErrorCode = Integer

type LocalizedText = Array<LocalizedString>

type SessionId = string
type Base64 = string

type Decimal = number
type Integer = number

type GUID = string

type TimeStamp = Integer

type Language = {
    key:LanguageCode
    isoCode?:string // https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
    name:string
}

namespace DocHelpers {

    export interface RequestResponseCouple {
        requestMessage: JSONMessage
        responseMessage: JSONMessage
    }

}

declare const dummyValue:any


//
// მეთოდის ტიპების ჩამონათვალი:
//
enum JSONMessageType {
    Request = 0,
    Response = 1,
    Notification = 2, // მაგალითად ჩეთის მესიჯი. სერვერი აგზავნის ChatMessage მესიჯს, რომელზეც response-ს არ ელის.
}

//
// საბაზო, პარამეტრების ობიექტი:
//
interface JSONMessageParams extends Object {
    sessionId?: string
}

declare function GenerateJSONMessageID(): number;
declare function CopyJSONMessageIDFromRequestMessage(): number;
declare function GenerateNewSessionId(): number

declare function findArrayElement<T>(arr:Array<T>, condition:((elem:T)=>boolean)):T

//
// მაგალითები
//
namespace Examples {

    //
    // JSON მესიჯის ობიექტის კონკრეტული მაგალითი:
    //
    var loginRequest = {
        method: 10, //JSONMethod.Login = 10
        params: {
            userName: "abcd",
            password: "!23ca"
        },
        messageId: 14,
        type: 0, //JSONMessageType.Request
        errorCode: 0
    }


    var loginResponse = {
        method: 11, //JSONMethod.LoginResult = 11
        params: {
            success: true,
            sessionId: "131232kfld",
        },
        messageId: 14,
        type: 1, //JSONMessageType.Request
        errorCode: 0
    }
}