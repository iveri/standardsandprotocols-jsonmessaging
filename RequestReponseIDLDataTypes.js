/// <reference path="JSONMessaging.ts" />
var JSONAPIImplementationStatus;
(function (JSONAPIImplementationStatus) {
    JSONAPIImplementationStatus[JSONAPIImplementationStatus["Finished"] = 0] = "Finished";
    JSONAPIImplementationStatus[JSONAPIImplementationStatus["Queued"] = 1] = "Queued";
    JSONAPIImplementationStatus[JSONAPIImplementationStatus["InProgress"] = 2] = "InProgress";
    JSONAPIImplementationStatus[JSONAPIImplementationStatus["FinishedNeedsRestructuring"] = 3] = "FinishedNeedsRestructuring";
    JSONAPIImplementationStatus[JSONAPIImplementationStatus["FinishedNeedUpdatesToComplyWithChanges"] = 4] = "FinishedNeedUpdatesToComplyWithChanges";
})(JSONAPIImplementationStatus || (JSONAPIImplementationStatus = {}));
