/// <reference path="standardsandprotocols.jsonmessaging/JSONMessaging.ts" />
/// <reference path="ObjectComparison.ts" />


namespace JSONMessaging {

    enum JSONMessageQueueItemState {
        Pending,
        Processing,
        Processed,
        ProcessedCached
    }

    export interface JSONPostMessageCallback
    {
        (responseMessage:JSONMessage):void;
    }

    interface JSONMessageQueueItem {
        jsonMessage: JSONMessage
        itemState: JSONMessageQueueItemState,
        jsonMessageResponseCallback: JSONPostMessageCallback
        cachedResponseMessage: JSONMessage
        shellCacheResponseMessage:Boolean
    }

    interface JSONMessageQueue extends Array<JSONMessageQueueItem>{}

    var messageQueue: JSONMessageQueue

    function getMessageQueuItemByMesageId(messageId:number):JSONMessageQueueItem
    {        
        for (var itemIndex = 0; itemIndex < messageQueue.length; itemIndex++)
        {
            var queueItem = messageQueue[itemIndex]

            if(queueItem.jsonMessage.messageId == messageId)
            {
                return queueItem
            }
        }
    }

    function makePOSTRequest_dummy(data:any, httpRequestSuccessHandler:Function, networkOrOtherSystemFailurehandler:Function)
    {
        // TODO: Implement AJAX request for runtime tests
    }

    function processJSONMessage(message: JSONMessage) {

        var queuItem = getMessageQueuItemByMesageId(message.messageId)
        queuItem.itemState = JSONMessageQueueItemState.Processing

        makePOSTRequest_dummy(JSON.stringify, 
            function(responseString:string){
                var responseMessage:JSONMessage = <JSONMessage>JSON.parse(responseString)
                var queueItem = getMessageQueuItemByMesageId(responseMessage.messageId)
                var requestMessage = queueItem.jsonMessage

                var requestMessageWasTransactional =  requestMessage.transaction !== undefined || requestMessage.transaction != null

                var messageWasTransactionalAndFinishedOrFailed = requestMessageWasTransactional
                && (requestMessage.transaction.transactionState == JSONMessageTransactionState.Failed 
                || requestMessage.transaction.transactionState == JSONMessageTransactionState.Finished)

                var messageWasTransactionalAndIsPending_ShellResetQueueItemToPendingStateForToTryToProcessItAgainInNextCycle =  requestMessageWasTransactional 
                && requestMessage.transaction.transactionState == JSONMessageTransactionState.Pending                

                if(requestMessageWasTransactional == false)
                {
                    queueItem.jsonMessageResponseCallback(responseMessage)

                    if(queueItem.shellCacheResponseMessage)
                    {
                        queueItem.itemState = JSONMessageQueueItemState.ProcessedCached
                        queueItem.cachedResponseMessage = responseMessage
                    }
                    else
                    {
                        queueItem.itemState = JSONMessageQueueItemState.Processed
                    }
                }
                else if(messageWasTransactionalAndFinishedOrFailed)
                {
                    queueItem.jsonMessageResponseCallback(responseMessage)

                    if(queueItem.shellCacheResponseMessage)
                    {
                        queueItem.itemState = JSONMessageQueueItemState.ProcessedCached

                        queueItem.cachedResponseMessage = responseMessage
                    }
                    else
                    {
                        queueItem.itemState = JSONMessageQueueItemState.Processed
                    }
                }
                else if(messageWasTransactionalAndIsPending_ShellResetQueueItemToPendingStateForToTryToProcessItAgainInNextCycle)
                {
                    queueItem.itemState = JSONMessageQueueItemState.Pending 
                }

            },
            // Swift  - Closures, Capturing values : https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Closures.html#//apple_ref/doc/uid/TP40014097-CH11-ID103
            // Java 8 - Lambda : http://viralpatel.net/blogs/lambda-expressions-java-tutorial/
            // Java   - Anonymous class: https://docs.oracle.com/javase/tutorial/java/javaOO/anonymousclasses.html 
            // C++    - Lambda : http://en.cppreference.com/w/cpp/language/lambda
            (function(requestMessageWhichFailedToTransmit:JSONMessage){ // Creating closure for capturing request message, cause we don't have reponse message with messageId to determine its "request" type message associated with it

                return function(error:any){

                    var queueItem = getMessageQueuItemByMesageId(requestMessageWhichFailedToTransmit.messageId)
                    var requestMessageWasTransactional = requestMessageWhichFailedToTransmit.transaction !== undefined || requestMessageWhichFailedToTransmit.transaction != null

                    if (requestMessageWasTransactional)
                    {
                        queueItem.itemState = JSONMessageQueueItemState.Pending
                    }
                    else 
                    {

                        if(queueItem.shellCacheResponseMessage)
                        {
                            queueItem.itemState = JSONMessageQueueItemState.ProcessedCached

                            queueItem.cachedResponseMessage = null
                        }
                        else
                        {
                            queueItem.itemState = JSONMessageQueueItemState.Processed
                        }                           
                        
                        queueItem.jsonMessageResponseCallback(null)

                    }
                     
                }
                
            })(message)
        )
    }

    function processPendingMessagesFromQueue(messageQueue: JSONMessageQueue) {
        for (var messageItemIndex = 0; messageItemIndex < messageQueue.length; messageItemIndex++) {
            var queueItem = messageQueue[messageItemIndex];

            var shellStartServerRequest = queueItem.itemState == JSONMessageQueueItemState.Pending

            if (shellStartServerRequest) {
                processJSONMessage(queueItem.jsonMessage)
            }

            var messageProcessedAndCanBeDeletedFromQueue = queueItem.itemState == JSONMessageQueueItemState.Processed

            if(messageProcessedAndCanBeDeletedFromQueue)
            {
                messageQueue.splice(messageItemIndex, 1) // Removes item from queue Array
            }
        }

        setTimeout(processPendingMessagesFromQueue, 1000, messageQueue)
    }

    function createJSONMessageQueueItem():JSONMessageQueueItem
    {
        var queuItem : JSONMessageQueueItem = {
            jsonMessage:null,
            itemState:JSONMessageQueueItemState.Pending,
            jsonMessageResponseCallback:null,
            cachedResponseMessage:null,
            shellCacheResponseMessage:false
        }

        return queuItem
    }

    function getCachedResponseMessageByRequestMethodIdAndParams(methodId:number, params:JSONMessageParams):JSONMessage
    {
        var _queueItem:JSONMessageQueueItem = null

        for(var queueItemIndex = 0; queueItemIndex < messageQueue.length; queueItemIndex++)
        {
            var queueItem = messageQueue[queueItemIndex]

            var messageParams = queueItem.jsonMessage.params

            if(queueItem.jsonMessage.method == methodId
            && objectsAreEqual(messageParams, params)
            && queueItem.cachedResponseMessage != null)
            {
                _queueItem = queueItem
            }
        }

        if(_queueItem)
        {
            return _queueItem.cachedResponseMessage
        }
        else
        {
            return null
        }
    }

    export function postJSONMessage(message: JSONMessage, callback: JSONPostMessageCallback, cacheResponse:boolean) {

        if(cacheResponse)
        {
            var methodId = message.method
            var params = message.params

            var cachedResponse = getCachedResponseMessageByRequestMethodIdAndParams(methodId, params)

            if(cachedResponse != null)
            {
                callback(cachedResponse)
                return
            }
        }

        var jsonMessageQueueItem = createJSONMessageQueueItem()

        jsonMessageQueueItem.itemState = JSONMessageQueueItemState.Pending
        jsonMessageQueueItem.jsonMessage = message
        jsonMessageQueueItem.jsonMessageResponseCallback = callback
        jsonMessageQueueItem.cachedResponseMessage = null
        jsonMessageQueueItem.shellCacheResponseMessage = cacheResponse

        messageQueue.push(jsonMessageQueueItem)

        processJSONMessage(message) //Initially, try processing immediatelly 
    }

    export function startMessageProcessing()
    {
        processPendingMessagesFromQueue(messageQueue)
    }

    export function initMessagingSystem()
    {
        messageQueue = new Array<JSONMessageQueueItem>()
    }


}

// postMessage გამოყენების მაგალითი

namespace Examples
{
    function messageCallback(message:JSONMessage):void
    {

    }

    var message = {
        method:JSONMethod.Login,
        params: {
            user: "Dasdas",
            pass: "jfdks9"
        },
        messageId: 123
    }

    export function testMessageProcessing()
    {
        JSONMessaging.initMessagingSystem()
        JSONMessaging.startMessageProcessing()

        JSONMessaging.postJSONMessage(message, function(responseMessage){

            console.log("response method: " + responseMessage.messageId)

        }, false)

        JSONMessaging.postJSONMessage(message, function(responseMessage){

            console.log("response method: " + responseMessage.messageId)

        }, true)

    }     
}


